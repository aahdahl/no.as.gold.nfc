package no.as.gold.nfc.view.models;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.UUID;

import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.view.utils.ObjectContainer;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;

/**
 * Created by aahdahl on 4/28/2014.
 */
public class CommandModel extends BaseModel {

    public static final String COMMAND_STRING = "COMMAND";
    public static final String HEX_COMMAND = "HEX_COMMAND";
    public static final String RESPONSE = "RESPONSE";
    public static final String TECH_LIST = "TECH_LIST";
    public static final String TAG_ID = "TAG_ID";
    private UUID uuid = UUID.randomUUID();

    //region Constructors
    public CommandModel() {
        // Register listeners for incoming ComHandlerMessages
        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(final ComHandlerMessage message) {
                // Only listen to the messages sent by me!
                if(uuid.equals(message.getSenderUUID())) {
                    setTagResponse(message.getReadMessage());
                }
            }
        });

    }


    //endregion

    //region Properties

    private ArrayList<String> Technologies;

    private String mCommandString;
    private String technology;
    private byte[] tagId;

    /**
     * Sets the command
     * @param command new value of command
     */
    public void setCommandString(String command) {
        // No notify or update if no change!
        if(command != null && command.equals(mCommandString)) return;

        // Verifies input, throws error if it does not work!
        setCommand(command);
        mCommandString = command;
        setChanged();
        notifyObservers();

    }

    /**
     * Gets the command
     * @return
     */
    public String getCommandString() {
        return mCommandString;
    }

    private byte[] mCommand;
    /**
     * Presents the command in hexidesimal form
     * @return Command in hexidecimal form
     */
    public String getFormattedCommand() {
        char[] hex = bytesToHex(mCommand);
        String hexFormat = "";

        if(mCommand == null || mCommand.equals("")) return "";

        for(int i = 0; i < hex.length; i+=2)
            hexFormat += "0x" + hex[i] + hex[i+1] + " ";
        return hexFormat;
    }

    /**
     * Updates the response received from the tag
     * @param value new tag response
     */
    private void setTagResponse(String value) {
        if( value != null && value.equals(mResponse)) return;
        mResponse = value;
        setChanged();
        notifyObservers(RESPONSE);
    }

    private String mResponse;
    /**
     * Gets the last response from the tag
     * @return Data received from the tag
     */
    public String getTagResponse() {
        return mResponse;
    }



    /**
     * This function transfers the command to the tag
     */
    public void sendCommand() throws Exception {
        NfcComHandler.ByteComHandler.transferCommand(ObjectContainer.Default.getTag(), getTechnology(), uuid, getCommand());
    }



    /**
     * Extracts the dot delimited bytes from the string and stores it as the command
     * @param command Dot delimited string with byte values
     * @throws InputMismatchException If values do not conform to bytes
     */
    private void setCommand(String command) throws InputMismatchException{
        if(command == null || command.equals("")) {
            mCommand = null;
        } else {

            String byteString[] = command.split("\\.");
            byte[] bytes = new byte[byteString.length];
            for(int i = 0; i < byteString.length; i++) {
                int intValue = Integer.parseInt(byteString[i]);
                // Input check
                if (intValue > 0xFF) throw new InputMismatchException();

                bytes[i] = (byte)intValue; // Converts unsigned int to signed byte
            }
            // All good with input string -> update command byte
            mCommand = bytes;
        }
        // inform observers that data is updated
        setChanged();
        notifyObservers(HEX_COMMAND);
    }

    /**
     * Gets the array of bytes that define the command
     * @return Command
     */
    private byte[] getCommand() {
        return mCommand;
    }


    /**
     * Sets the currently selected technology
     * @param technology technology selected
     */
    public void setTechnology(String technology) {
        if(technology != null && technology.equals(this.technology)) return;
        this.technology = technology;
        setChanged();
        notifyObservers();
    }

    /**
     * Gets the currently selected technology
     * @return technology selected
     */
    public String getTechnology() {
        return technology;
    }

    /**
     * Gets the list of technologies
     * @return list of technologies
     */
    public ArrayList<String> getTechnologies() {
        return Technologies;
    }

    /**
     * Sets the available list of technologies
     * @param technologies list of technologies
     */
    public void setTechnologies(ArrayList<String> technologies) {
        Technologies = technologies;
        setChanged();
        notifyObservers(TECH_LIST);
    }

    //endregion

    //region Private methods

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static char[] bytesToHex(byte[] bytes) {
        if(bytes == null) return null;

        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return hexChars;
    }

    public byte[] getTagId() {
        return tagId;
    }

    public void setTagId(byte[] tagId) {
        this.tagId = tagId;
        setChanged();
        notifyObservers(TAG_ID);

    }

    //endregion
}
