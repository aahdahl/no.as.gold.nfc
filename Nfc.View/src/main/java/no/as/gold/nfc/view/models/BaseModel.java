package no.as.gold.nfc.view.models;

import java.util.Observable;

/**
 * This is a base model that ensures that the model updates listeners with changes
 * Created by aahdahl on 4/28/2014.
 */
public class BaseModel extends Observable {

}
