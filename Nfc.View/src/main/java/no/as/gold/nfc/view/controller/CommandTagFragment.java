package no.as.gold.nfc.view.controller;

import android.content.Context;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import no.as.gold.nfc.view.R;
import no.as.gold.nfc.view.messages.NewIntentMessage;
import no.as.gold.nfc.view.models.CommandModel;
import no.as.gold.nfc.view.utils.ObjectContainer;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

/**
 * Created by aahdahl on 4/27/2014.
 */
public class CommandTagFragment extends BaseTagFragment  implements Observer {

    private EditText mBytesTextView;
    private View mRootView;
    private TextView mResponseTextView;
    private TextView mSentTextView;
    private TextView mTagIdTextView;
    private Button mCommandButton;
    private ListView mTechList;
    private Context mContext;
    private CommandModel mModel = new CommandModel();

    //region Fragment overrides
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_command_tag,
                container, false);

        mResponseTextView = (TextView)mRootView.findViewById(R.id.command_response_text_view);
        mSentTextView = (TextView)mRootView.findViewById(R.id.command_sent_text_view);
        mTagIdTextView = (TextView)mRootView.findViewById(R.id.command_tag_id_text_view);
        mCommandButton = (Button)mRootView.findViewById(R.id.command_tag_button);
        mBytesTextView = (EditText)mRootView.findViewById(R.id.command_bytes_text_view);
        mTechList = (ListView)mRootView.findViewById(R.id.tech_list);

        mTechList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mModel.setTechnology(((TextView) view).getText().toString());
            }
        });

        mContext = mRootView.getContext();

        // Set button listener
        mCommandButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Read tag content and output it to mReadTextView
                try {
                    mModel.sendCommand();
                } catch (Exception e) {
                    MessengerService.Default.send(new ErrorMessage(e.getMessage()));
                }
            }
        });

        // set input listener
        mBytesTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    mModel.setCommandString(s.toString());
                    mBytesTextView.setBackgroundColor(0x00000000);  // set color to red
                } catch (Exception e) {
                    MessengerService.Default.send(new InformationMessage("Input error!"));
                    mBytesTextView.setBackgroundColor(0xfff00000);  // set color to red
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Check for new tag intents
        MessengerService.Default.Register(this, NewIntentMessage.class, new MessageHandler<NewIntentMessage>() {
            @Override
            public void handle(final NewIntentMessage msg) {
                populateView();
            }
        });

        mModel.addObserver(this);

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        populateView();
    }


    private void populateView() {
        Tag tag = ObjectContainer.Default.getTag();
        if(tag == null) return;

        // Update tech list
        mModel.setTechnologies(new ArrayList<>(Arrays.asList(tag.getTechList())));
        mModel.setTagId(tag.getId());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mModel.deleteObserver(this);
        MessengerService.Default.UnRegister(this, NewIntentMessage.class);
    }

    /**
     * Parses input string and sends it to the tag
     */
    private void sendCommand() {
        try {
            String input = mBytesTextView.getText().toString();
            if(input == null || input.isEmpty()) {
                MessengerService.Default.send(new InformationMessage("Empty command!"));
                return;
            }

            String byteString[] = input.split("\\.");
            byte[] bytes = new byte[byteString.length];
            for(int i = 0; i < byteString.length; i++) {
                int intValue = Integer.parseInt(byteString[i]);
                // Input check
                if (intValue > 0xFF) {
                    MessengerService.Default.send(new ErrorMessage("Illegal byte value: " + byteString[i]));
                    return;
                }
                bytes[i] = (byte)intValue; // Converts unsigned int to signed byte
            }
            //NfcComHandler.ByteComHandler.transferCommand(GetTag(), mSelectedTech, mUUID, bytes);
        } catch (Exception e) {
            MessengerService.Default.send(new ErrorMessage(e.toString()));
        }
    }

    @Override
    public void update(Observable observable, Object data) {

        if(data instanceof String) {
            String field = (String)data;

            switch (field) {
                // Update hex string
                case CommandModel.HEX_COMMAND:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSentTextView.setText(getResources().getString(R.string.command_text_view) + " " + mModel.getFormattedCommand());
                        }
                    });
                    break;
                // Update response
                case CommandModel.RESPONSE:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mResponseTextView.setText(getResources().getString(R.string.command_response_text_view) + " " + mModel.getTagResponse());
                        }
                    });
                    break;
                // Update tech list
                case CommandModel.TECH_LIST:
                    // Set new adapter and add list of items
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mTechList.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, mModel.getTechnologies()));
                        }
                    });
                    break;
                case CommandModel.TAG_ID:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mTagIdTextView.setText(getResources().getString(R.string.command_tag_id_text_view) + " " + Arrays.toString(mModel.getTagId()));
                        }
                    });
                    break;
                // Update response
                default:

            }
        }
    }

    //endregion
}
