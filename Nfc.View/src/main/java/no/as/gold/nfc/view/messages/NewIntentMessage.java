package no.as.gold.nfc.view.messages;

import android.content.Intent;

import no.as.gold.simplemessenger.messages.IMessage;

/**
 * This is a message that should be sent every time the main activity receives a new intent.
 * The objective is to inform fragments that a new tag is nearby
 * @author aage
 *
 */
public class NewIntentMessage implements IMessage {

    /**
     * Intent that caused this message to be sent
     */
    public android.content.Intent Intent;

    /**
     * Constructor for NewIntentMessage
     * @param intent Intent that caused this message to be sent
     */
    public NewIntentMessage(Intent intent) {
        Intent = intent;
    }

    @Override
    public String getMessage() {
        // TODO Auto-generated method stub
        return null;
    }

}
