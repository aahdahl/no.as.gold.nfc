package no.as.gold.nfc.view.controller;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import no.as.gold.nfc.communication.NfcComHandler;
import no.as.gold.nfc.communication.Utils.Identifiers;
import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.view.R;
import no.as.gold.nfc.view.messages.NewIntentMessage;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;

/**
 * This class performs binary read operations from tags
 * Created by Aage Dahl on 14.02.14.
 */
public class BWriteTagFragment extends BaseTagFragment {
    private EditText mAddresTextView;
    private EditText mBytesTextView;
    private TextView mReadTextView;
    private View mRootView;
    private Button mWriteButton;
    private String mSelectedTech;

    private Collection<TextView> mTechs = new ArrayList<TextView>();
    private ListView mTechList;
    private Context mContext;
    private NfcAdapter mAdapter;
    private UUID mUUID = UUID.fromString(Identifiers.BWriterFragmentID);

    //region Fragment overrides
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initiate view
        mRootView = inflater.inflate(R.layout.fragment_bwrite_tag,
                container, false);

        mReadTextView = (TextView)mRootView.findViewById(R.id.bwrite_responce_edit_text);
        mWriteButton = (Button)mRootView.findViewById(R.id.bwrite_tag_button);
        mAddresTextView = (EditText)mRootView.findViewById(R.id.bwrite_address_text_view);
        mBytesTextView = (EditText)mRootView.findViewById(R.id.bwrite_bytes_text_view);
        mTechList = (ListView)mRootView.findViewById(R.id.tech_list);
        mTechList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectedTech = ((TextView)view).getText().toString();
            }
        });

        mContext = mRootView.getContext();

        // Get nfc adapter
        mAdapter = NfcAdapter.getDefaultAdapter(mContext);

        // Set button listener
        mWriteButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Read tag content and output it to mReadTextView
                writeTag();
            }
        });

        // Register listeners for incoming ComHandlerMessages
        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(final ComHandlerMessage message) {
                // Only listen to the messages sent by me!
                if(mUUID.equals(message.getSenderUUID())) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mReadTextView.setText(message.getMessage());
                        }
                    });
                }
            }
        });

        // Listen for new tags
        MessengerService.Default.Register(this, NewIntentMessage.class, new MessageHandler<NewIntentMessage>() {
            @Override
            public void handle(final NewIntentMessage msg) {
                // Update tech list
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Tag tag = msg.Intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                        if(tag == null) return;

                        // Set new adapter and add list of items
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, tag.getTechList());
                        mTechList.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, tag.getTechList()));
                    }
                });
            }
        });

        return mRootView;
    }

    private void writeTag() {
        try {
            String input = mBytesTextView.getText().toString();
            String byteString[] = input.split("\\.");
            byte[] bytes = new byte[byteString.length];
            for(int i = 0; i < byteString.length; i++) {
                bytes[i] = Byte.parseByte(byteString[i]);
            }
            int offset = Integer.valueOf(mAddresTextView.getText().toString());
            NfcComHandler.ByteComHandler.Write(GetTag(), mSelectedTech, mUUID, bytes, offset);
        } catch (Exception e) {
            MessengerService.Default.send(new ErrorMessage(e.toString()));
        }
    }

    //endregion

}
