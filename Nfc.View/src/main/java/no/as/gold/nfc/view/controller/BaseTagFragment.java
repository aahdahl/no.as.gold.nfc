package no.as.gold.nfc.view.controller;

import android.nfc.Tag;
import android.support.v4.app.Fragment;

import java.util.Observable;
import java.util.Observer;

import no.as.gold.nfc.view.utils.ObjectContainer;

/**
 * This is a base class for any fragment that works with tags. It extends the {@link android.support.v4.app.Fragment} with functionality to run tag intents.
 * Created by aage on 16.12.13.
 */
public abstract class BaseTagFragment extends Fragment {
    //region fields
    private Tag mTag;
    //endregion

    //region Constructors
    /**
     * Constructor that initiates the BaseTagFragment
     */
    public BaseTagFragment() {
        // Add message listeners
        registerMessageListeners();
    }
    //endregion

    //region Properties
    public Tag GetTag() {return ObjectContainer.Default.getTag();} //mTag;}
    //endregion

    //region Private methods
    private void registerMessageListeners() {
    }
    //endregion
}
