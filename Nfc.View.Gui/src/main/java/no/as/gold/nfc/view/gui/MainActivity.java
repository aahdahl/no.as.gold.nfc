package no.as.gold.nfc.view.gui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import no.as.gold.nfc.communication.messages.ComHandlerMessage;
import no.as.gold.nfc.view.controller.BReadTagFragment;
import no.as.gold.nfc.view.controller.BWriteTagFragment;
import no.as.gold.nfc.view.controller.CommandTagFragment;
import no.as.gold.nfc.view.messages.NewIntentMessage;
import no.as.gold.nfc.view.utils.ObjectContainer;
import no.as.gold.simplemessenger.MessageHandler;
import no.as.gold.simplemessenger.MessengerService;
import no.as.gold.simplemessenger.messages.ErrorMessage;
import no.as.gold.simplemessenger.messages.InformationMessage;

import static android.widget.Toast.makeText;


public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    //region Static fields
    public static final String MIME_TEXT_PLAIN = "text/plain";
    private String mLogText = "";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    private Context mContext;
    private Toast mToaster;
    private NfcAdapter mAdapter;
    private TextView mLogTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);

        // Area for displaying logged information
        mLogTv = (TextView) findViewById(R.id.log_text_view);

        // Get context and extract toaster
        mContext = getApplicationContext();
        // Make context available for the rest of the app.
        ObjectContainer.Default.setAppContext(mContext);
        mToaster = makeText(mContext, "", Toast.LENGTH_SHORT);

        // Prepare tabs
        refreshActionBar();

        // Initiate last intent
        onNewIntent(getIntent());

        // Check NFC compatibility
        checkAndAssignNfcAdapter();

        // Register listeners for messages
        registerMessageListeners();


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        MessengerService.Default.UnRegisterAll();
        if (mToaster != null)
            mToaster.cancel();
    }

    @Override
    protected void onNewIntent(Intent intent) {

        // In case NFC adapter is not assigned (if Nfc adapter was turned off)
        if (mAdapter != null) checkAndAssignNfcAdapter();

        MessengerService.Default.send(new NewIntentMessage(intent));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForegroundDispatch(this, mAdapter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopForegroundDispatch(this, mAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //region Private methods

    /**
     * @param activity
     * @param adapter
     */
    private void stopForegroundDispatch(Activity activity,
                                        NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);

    }

    /**
     * Prevents a new instance of this activity to start for every intent received
     *
     * @param activity
     * @param adapter
     */
    private void setupForegroundDispatch(final Activity activity,
                                         NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);
        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};
        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }
//        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
        adapter.enableForegroundDispatch(activity, pendingIntent, null, null);
    }

    // Updates tabs
    private void refreshActionBar() {
        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Cleanup
        actionBar.removeAllTabs();

        // Add new tabs
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        // Need to clear all existing fragments from fragmentManager before populating new ones ( or else it remembers old fragments)
        //if (getSupportFragmentManager().getFragments() != null) getSupportFragmentManager().getFragments().clear();
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager
                .setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        actionBar.setSelectedNavigationItem(position);
                    }
                });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(actionBar.newTab()
                    .setText(mSectionsPagerAdapter.getPageTitle(i))
                    .setTabListener(this));
        }

        mSectionsPagerAdapter.notifyDataSetChanged();

    }

    private void registerMessageListeners() {
        // Handle com messages
        MessengerService.Default.Register(this, ComHandlerMessage.class, new MessageHandler<ComHandlerMessage>() {
            @Override
            public void handle(ComHandlerMessage msg) {
                final String toastMessage;
                switch (msg.Status) {
                    case COMMAND_SUCCESS:
                        toastMessage = "Response: " + msg.getReadMessage();
                        break;
                    case COMMAND_FAILED:
                        toastMessage = "Error transmitting command.\n" + msg.getMessage();
                        break;
                    case READ_SUCCESS:
                        toastMessage = "Read: " + msg.getReadMessage();
                        break;
                    case READ_FAILED:
                        toastMessage = "Error reading from tag.\n" + msg.getMessage();
                        break;
                    case WRITE_SUCCESS:
                        toastMessage = "Successfully wrote to tag.\n" + msg.getMessage();
                        break;
                    case WRITE_FAILED:
                        toastMessage = "Error writing to tag.\n" + msg.getMessage();
                        break;
                    case CONNECTION_ERROR:
                        toastMessage = "Unable to connect to tag.\n" + msg.getMessage();
                        break;
                    default:
                        toastMessage = "Unknown communication message received.\n" + msg.getMessage();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mToaster.setText(toastMessage);
                        mToaster.show();
                    }
                });
            }
        });

        // Handle info messages
        MessengerService.Default.Register(this, InformationMessage.class, new MessageHandler<InformationMessage>() {
            @Override
            public void handle(final InformationMessage msg) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mToaster.setText(msg.getMessage());
                        mToaster.show();
                    }
                });
            }
        });

        // Handle error messages
        MessengerService.Default.Register(this, ErrorMessage.class, new MessageHandler<ErrorMessage>() {
            @Override
            public void handle(ErrorMessage msg) {
                SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");
                Date now = new Date();
                String strDate = sdfDate.format(now);

                mLogText = strDate + ": " + msg.getMessage() + "\n" + mLogText;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLogTv.setText(mLogText);
                    }
                });
            }
        });
    }

    private void checkAndAssignNfcAdapter() {
        mAdapter = NfcAdapter.getDefaultAdapter(getApplicationContext());
        if (mAdapter == null) {
            mToaster.setText("This device does not support NFC");
            mToaster.show();
            finish();
            return;
        } else if (!mAdapter.isEnabled()) {
            mToaster.setText("NFC is disabled!");
            mToaster.show();
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    //endregion

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final CharSequence READ_TAB = "READ";
        private final CharSequence WRITE_TAB = "WRITE";
        private final CharSequence COMMAND_TAB = "COMMAND";

        // Collection of fragments
        ArrayList<Fragment> mFragments = new ArrayList<Fragment>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragments.add(new BReadTagFragment());
            mFragments.add(new BWriteTagFragment());
            mFragments.add(new CommandTagFragment());
        }

        @Override
        public Fragment getItem(int i) {
            if (i > getCount() - 1)
                return null;
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return READ_TAB;
                case 1:
                    return WRITE_TAB;
                case 2:
                    return COMMAND_TAB;
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}
